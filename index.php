<?php

use Phalcon\Mvc\View\Engine\Volt;

try
{
    //Create a DI
    $di = new Phalcon\DI\FactoryDefault();


// Register Volt as a service
    $di->set(
        "voltService",
        function ($view, $di) {
            $volt = new Volt($view, $di);

            $volt->setOptions(
                [
                    "compiledPath"      => "../cache/volt/",
                ]
            );

            return $volt;
        }
    );
    //Register an autoloader
    $loader = new \Phalcon\Loader();
    $loader->registerDirs(array(
        '../app/controllers/',
        '../app/models/'
    ))->register();


    //Setting up the view component
    $di->set('view', function(){
        $view = new \Phalcon\Mvc\View();
        $view->setViewsDir('../app/views/');
        $view->registerEngines([
            '.volt'  => 'voltService',
        ]);
        return $view;
    });

    //Set the database service
    $di->set('db', function(){
        return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            "host" => "localhost",
            "username" => "root",
            "password" => "21",
            "dbname" => "myproject"
        ));
    });

    //Handle the request
    $application = new \Phalcon\Mvc\Application();
    $application->setDI($di);
    echo $application->handle()->getContent();

}
catch(\Phalcon\Exception $e)
{
    echo "PhalconException: ", $e->getMessage();
}